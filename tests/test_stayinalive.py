import datetime
import json
import threading
import time

import stayinalive


def test_tests_directory_not_exists(tmp_path, capsys):
    tests_directory = tmp_path.joinpath("not-exists")
    mode_file = tmp_path.joinpath("mode-file")
    status_file = tmp_path.joinpath("status-file")

    stayinalive.stayinalive(tests_directory, mode_file, status_file, threading.Event())

    assert capsys.readouterr().out == f"exiting: {tests_directory} must exist and be a directory\n"
    assert not status_file.exists()


def test_tests_directory_not_a_directory(tmp_path, capsys):
    tests_directory = tmp_path.joinpath("file")
    tests_directory.touch()
    mode_file = tmp_path.joinpath("mode-file")
    status_file = tmp_path.joinpath("status-file")

    stayinalive.stayinalive(tests_directory, mode_file, status_file, threading.Event())

    assert capsys.readouterr().out == f"exiting: {tests_directory} must exist and be a directory\n"
    assert not status_file.exists()


PROGRESSIVE_AND_KO_INPUT = """
---
- test: false
  fix: true
  order: 1
""".lstrip()

PROGRESSIVE_AND_KO_OUTPUT_1 = """
watching: {0}, {1}
tests directory has changed
reading configuration
validating tests
""".lstrip()

PROGRESSIVE_AND_KO_OUTPUT_2 = """
tests directory has changed
reading configuration
validating tests
tests have changed
filtering active tests
active: {"test": "false", "fix": "true", "order": 1}
[1] failed: false
[1] fixing: true
[1] failed again: false
exiting: fatal error
""".lstrip()


def test_progressive_and_ko(tmp_path, capsys):
    # empty configuration
    tests_directory = tmp_path.joinpath("tests")
    tests_directory.mkdir()
    mode_file = tmp_path.joinpath("mode-file")
    status_file = tmp_path.joinpath("status-file")

    # here we run stayinalive() in a thread to be able to capture the output and add a test file while it's running
    thread = threading.Thread(target=stayinalive.stayinalive, args=(tests_directory, mode_file, status_file, threading.Event(), 0.1))
    thread.start()

    time.sleep(0.15)  # 2 cycles
    assert json.loads(status_file.read_text()).get("status") == "ok"
    assert capsys.readouterr().out == PROGRESSIVE_AND_KO_OUTPUT_1.format(tests_directory, mode_file)

    # adding failing test
    tests_directory.joinpath("failing-test").write_text(PROGRESSIVE_AND_KO_INPUT)

    thread.join()
    assert json.loads(status_file.read_text()).get("status") == "ko"
    assert capsys.readouterr().out == PROGRESSIVE_AND_KO_OUTPUT_2

    # previous run failed, so if we start it again, it should immediately fail
    stayinalive.stayinalive(tests_directory, mode_file, status_file, threading.Event())
    assert capsys.readouterr().out == f"exiting: ko status found in {status_file}\n"


OK_INPUT = """
---
- test: "[ ! -f {0} ]"
  fix: rm {0}
  order: 1
  mode: nominal
""".lstrip()

OK_OUTPUT = """
watching: {0}, {1}
tests directory has changed
reading configuration
validating tests
tests have changed
active mode has changed: nominal
filtering active tests
active: {{"test": "[ ! -f {2} ]", "fix": "rm {2}", "order": 1, "mode": ["nominal"]}}
[1] failed: [ ! -f {2} ]
[1] fixing: rm {2}
[1] fix successful
[1] failed: [ ! -f {2} ]
[1] fixing: rm {2}
[1] fix successful
exiting: loop-ending signal
""".lstrip()


def test_ok(tmp_path, capsys):
    # regular configuration
    flag = tmp_path.joinpath("flag")
    tests_directory = tmp_path.joinpath("tests")
    tests_directory.mkdir()
    tests_directory.joinpath("test-file").write_text(OK_INPUT.format(flag))
    mode_file = tmp_path.joinpath("mode-file")
    mode_file.write_text("nominal")
    status_file = tmp_path.joinpath("status-file")
    event = threading.Event()

    thread = threading.Thread(target=stayinalive.stayinalive, args=(tests_directory, mode_file, status_file, event, 0.03))
    thread.start()

    # lots of cycles
    time.sleep(0.2)
    status_1 = json.loads(status_file.read_text())
    assert status_1.get("status") == "ok"

    # first problem
    flag.touch()
    time.sleep(0.1)
    status_2 = json.loads(status_file.read_text())
    assert status_2.get("status") == "ok"
    assert datetime.datetime.fromisoformat(status_2.get("timestamp")) > datetime.datetime.fromisoformat(status_1.get("timestamp"))

    # second problem
    flag.touch()
    time.sleep(0.2)
    status_3 = json.loads(status_file.read_text())
    assert status_3.get("status") == "ok"
    assert datetime.datetime.fromisoformat(status_3.get("timestamp")) > datetime.datetime.fromisoformat(status_2.get("timestamp"))

    # normal interruption
    event.set()
    thread.join()

    assert capsys.readouterr().out == OK_OUTPUT.format(tests_directory, mode_file, flag)
