import json
import os
import signal
import threading
import time

import stayinalive

OK_OUTPUT = """
watching: {0}, {1}
tests directory has changed
reading configuration
validating tests
exiting: loop-ending signal
""".lstrip()


def test_ok(tmp_path, capsys):
    tests_directory = tmp_path.joinpath("conf")
    tests_directory.mkdir()
    mode_file = tmp_path.joinpath("mode-file")
    status_file = tmp_path.joinpath("status.json")

    def delayed_kill():
        time.sleep(0.5)
        os.kill(os.getpid(), signal.SIGINT)

    threading.Thread(target=delayed_kill).start()

    stayinalive.main(["-t", str(tests_directory), "-m", str(mode_file), "-s", str(status_file), "-d", "0.2"])

    assert capsys.readouterr().out == OK_OUTPUT.format(tests_directory, mode_file)
    assert json.loads(status_file.read_text(encoding="utf-8")).get("status") == "ok"
